<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">
        Employee Payroll Management
      </li>
      <li>
        <a href="employees">
          <i class="fa fa-users"></i> <span>Manage Employees</span>
        </a>
      </li>
      <li>
        <a href="offices">
          <i class="fa fa-envelope"></i> <span>Manage Offices</span>
        </a>
      </li>
      <li>
        <a href="payroll">
          <i class="fa fa-envelope"></i> <span>Manage Payroll</span>
        </a>
      </li>
      <li>
        <a href="payslip">
          <i class="fa fa-envelope"></i> <span>Manage Payslip</span>
        </a>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>