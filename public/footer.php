  <!-- footer here -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?php echo 2018; ?>.</strong> All rights
    reserved.
  </footer>
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="../assets/plugins/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="../assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- Select2 -->
  <script src="../assets/plugins/select2/dist/js/select2.full.min.js"></script>
  <!-- PACE -->
  <script src="../assets/plugins/pace/pace.min.js"></script>
  <!-- DataTables -->
  <script src="../assets/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="../assets/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="../assets/theme/js/adminlte.min.js"></script>

  <script type="text/javascript">
    (function(event) {

      $(document).ajaxStart(function() {
        Pace.restart()
      })

      //select 2
      $('.select2').select2();

      // init user table
      $('#dataTable').DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        'autoWidth': true
      })

      var checked = 0;
      $('button[data-role="confirm_delete_employee"]').click(function(event) {
        event.preventDefault();
        var id = $(this).data('employeeid');
        $('#employee_id').val(id);
        $('#delete_employee-form').modal('toggle');
      });

      $('button[data-role="confirm_delete_office"]').click(function(event) {
        event.preventDefault();
        var id = $(this).data('officeid');
        $('#office_id').val(id);
        $('#delete_office-form').modal('toggle');
      });

      $('button[data-role="confirm_delete_payroll"]').click(function(event) {
        event.preventDefault();
        var id = $(this).data('payrollid');
        $('#payroll_id').val(id);
        $('#delete_payroll-form').modal('toggle');
      });

      $('button[data-role="confirm_delete_payslip"]').click(function(event) {
        event.preventDefault();
        var id = $(this).data('payslipid');
        $('#payslip_id').val(id);
        $('#delete_payslip-form').modal('toggle');
      });

    })(jQuery);
  </script>
  <script type="text/javascript">
    function get_email() {
      var contact_number = document.getElementById("select_receiver").value;
      $('#receiver_email').val(contact_number);
    }
  </script>

  </body>

  </html>