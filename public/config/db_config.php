<?php

    $host = "localhost";
    $db_name = "employee_payroll_db";
    $username = "root";
    $password = "";

    try{

        $con = new PDO("mysql:host=$host; dbname=$db_name", $username, $password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION) );
        //initialize error exception
        $con->exec("set names utf8");
        // echo 'connected';
  
    }catch(PDOEXCEPTION $error){

        echo "Connection Error: " . $error->getMessage();
        
    }