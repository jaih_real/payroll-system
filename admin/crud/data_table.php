<?php

  function payroll_datatable($office_id){
    $employees = fetch_all_office_employees($office_id);
    $tbl = '';
    $tbl .= '  
      <div style="width:100%;"> 
        <table  cellpadding="5" cellspacing="1" border="1" style="text-align:center;"> 
          <tr >
            <th style="width:20%;">NAME</th>
            <th style="width:20%;">POSITION</th>
            <th style="width:8%;">BASIC</th>
            <th style="width:8%;">GSIS</th>
            <th style="width:8%;">WTAX</th>
            <th style="width:8%;">LOANS</th>
            <th style="width:8%;">TOTALDEC</th>
            <th style="width:10%;">NETPAY</th>
            <th style="width:10%;">SIGNATURE</th>
          </tr>
      <tbody>
    '; 

    foreach ($employees as $employee) {
      # code...
      $tbl .= '
        <tr>
          <td align="left">'.$employee['full_name'].'</td>
          <td align="left">'.$employee['position'].'</td>
          <td align="left">'.$employee['basic'].'</td>
          <td align="left">'.$employee['gsis'].'</td>
          <td align="left">'.$employee['wtax'].'</td>
          <td align="left">'.$employee['loans'].'</td>
          <td align="left">'.$employee['totaldec'].'</td>
          <td align="left">'.$employee['net_pay'].'</td>
          <td align="left">'.''.'</td>
        </tr>
      ';
    }
    $tbl .='
          </tbody>
        </table>
      </div>
    ';

    return $tbl;
  }

  function employee_datatable()
  {
    $employees = fetch_payslip();
    // $offices = fetch_offices();
    // foreach ($offices as $office) {
      $tbl = '';
      
      $tbl = '
          <table cellpadding="5" border="0">
            <tr>
              <td colspan="3" align="center"><u><h1>EMPLOYEE REPORT</b></h1></td>
            </tr>
          </table>
      ';

      // $employees = fetch_all_office_employees($office['id']);
      $tbl .= '  
          <div style="width:100%;"> 
            <table  cellpadding="5" cellspacing="1" border="1" style="text-align:center;"> 
              <tr >
                <th style="width:15%;">NAME</th>
                <th style="width:15%;">POSITION</th>
                <th style="width:10%;">OFFICE</th>
                <th style="width:7%;">PAY CODE</th>
                <th style="width:8%;">BASIC</th>
                <th style="width:8%;">GSIS</th>
                <th style="width:8%;">WTAX</th>
                <th style="width:8%;">LOANS</th>
                <th style="width:10%;">TOTALDEC</th>
                <th style="width:10%;">NETPAY</th>
              </tr>
          <tbody>
        ';

      foreach ($employees as $employee) {
        $tbl .= '
            <tr>
              <td align="left">' . $employee['full_name'] . '</td>
              <td align="left">' . $employee['position'] . '</td>
              <td align="left">' . $employee['name'] . '</td>
              <td align="left">' . $employee['pay_code'] . '</td>
              <td align="center">' . $employee['basic'] . '</td>
              <td align="center">' . $employee['gsis'] . '</td>
              <td align="center">' . $employee['wtax'] . '</td>
              <td align="center">' . $employee['loans'] . '</td>
              <td align="center">' . $employee['totaldec'] . '</td>
              <td align="center">' . $employee['net_pay'] . '</td>
            </tr>
          ';
      }
      $tbl .= '
              </tbody>
            </table>
          </div>
        ';
      return $tbl;
    // }
  }

  function office_datatable()
  {
    $offices = fetch_offices();

    $tbl = '';
    $tbl = '
        <table cellpadding="5" border="0">
          <tr>
            <td colspan="3" align="center"><u><h1>OFFICE REPORT</b></h1></td>
          </tr>
        </table>
    ';

    $tbl .= '  
        <div style="width:100%;"> 
          <table  cellpadding="5" cellspacing="1" border="1" style="text-align:center;"> 
            <tr >
              <th style="width:70%;">OFFICE</th>
              <th style="width:30%;">PAY CODE</th>
            </tr>
        <tbody>
      ';

    foreach ($offices as $office) {
      $tbl .= '
          <tr>
            <td align="left">' . $office['name'] . '</td>
            <td align="center">' . $office['pay_code'] . '</td>
            <td align="left">' . '' . '</td>
          </tr>
        ';
    }
    $tbl .= '
            </tbody>
          </table>
        </div>
      ';
    return $tbl;
  }

  function payslip_datatable($employee_id)
  {
    $employee = fetch_office_employee($employee_id);
    $tbl = '';
    $tbl .= '
			<table cellpadding="5" border="0">
				<tr>
					<td colspan="3" align="center"><u><h1>EMPLOYEE PAYSLIP REPORT</b></h1></td>
				</tr>
				<tr>
					<td style="width:15%;" align="left"><b>PAYROLL ' . $employee['pay_code'] . '</b></td>
				</tr>
				<tr>
					<td></td>
				</tr>
				<tr>
					<td align="left"><b>OFFICE</b></td>
					<td style="width:3%;" align="left"><b>:</b></td>
					<td align="left"><b>' . $employee['name'] . '</b></td>
				</tr>
				<tr>
					<td align="left"><b>PAYROLL CODE</b></td>
					<td align="left"><b>:</b></td>
					<td align="left"><b>' . $employee['pay_code'] . '</b></td>
				</tr>
			</table>
		';
    $tbl .= '  
      <div style="width:100%;"> 
        <table  cellpadding="5" cellspacing="1" border="1" style="text-align:center;"> 
          <tr >
            <th style="width:20%;">NAME</th>
            <th style="width:20%;">POSITION</th>
            <th style="width:8%;">BASIC</th>
            <th style="width:8%;">GSIS</th>
            <th style="width:8%;">WTAX</th>
            <th style="width:8%;">LOANS</th>
            <th style="width:8%;">TOTALDEC</th>
            <th style="width:10%;">NETPAY</th>
            <th style="width:10%;">SIGNATURE</th>
          </tr>
      <tbody>
    '; 

    $tbl .= '
      <tr>
        <td align="left">'.$employee['full_name'].'</td>
        <td align="left">'.$employee['position'].'</td>
        <td align="left">'.$employee['basic'].'</td>
        <td align="left">'.$employee['gsis'].'</td>
        <td align="left">'.$employee['wtax'].'</td>
        <td align="left">'.$employee['loans'].'</td>
        <td align="left">'.$employee['totaldec'].'</td>
        <td align="left">'.$employee['net_pay'].'</td>
        <td align="left">'.''.'</td>
      </tr>
    ';
    $tbl .='
          </tbody>
        </table>
      </div>
    ';

    return $tbl;
  }