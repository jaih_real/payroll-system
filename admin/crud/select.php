<?php
	
	// fetch means get only 1 array of result
	// fetchAll means get all array of data

	// fetch employee
	function fetch_employees(){
		global $con;
		try {
			$sql = 'SELECT * FROM employees';
			$statement = $con->prepare($sql);
			$statement->execute();
			return $statement->fetchAll(PDO::FETCH_ASSOC);
			
		} catch (Exception $e) {
			echo 'A PDO Exeption happened';
		}
	}

	// fetch employee by id
	function get_employee_by_id($id){

	}

	// fetch offices
	function fetch_offices(){

	}

	// fetch offices by id
	function get_office_by_id($id)
	{

	}
	
	// fetch payroll by id
	function get_payroll_by_id($id) // need office_id
	{

	}

	// fetch all employee that has the same office_id
	function fetch_all_office_employees($id) // need office_id
	{

	}

	// fetch employee that has the same employee_id
	function fetch_office_employee($id) // needs employee_id
	{

	}

	// fetch payslip
	function fetch_payslip(){

	}

	// fetch payslip by id
	function get_payslip_by_id($id){

	}

	// check if office_id is used in employee table
	function check_office_id($id){

	}