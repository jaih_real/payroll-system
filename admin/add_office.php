<?php

// only require the file once
// require_once throw fatal error and stop the code
// include throw a warning and continue code execution 
require_once '../public/config/db_config.php';
require_once '../public/header.php';
require_once '../public/sidebar.php';
require_once 'crud/insert.php';
require_once 'crud/select.php';

$alert_msg = '';

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add Office
      <!-- <small>Version 2.0</small> -->
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-10">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Office Details</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="post" action="<?php htmlspecialchars("PHP_SELF"); ?>">
            <div class="box-body">
              <!-- code here -->
              <!-- CREATE HTML -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <!-- CREATE BUTTON SAVE -->
              <!-- CREATE BUTTON BACK -->
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <div class="col-md-1"></div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<?php

require_once '../public/footer.php';


?>